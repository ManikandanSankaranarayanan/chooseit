DROP DATABASE IF EXISTS Chooseit;
CREATE DATABASE Chooseit;

Use Chooseit;

DROP TABLE IF EXISTS `Chooseit`.`categories`;
CREATE TABLE `Chooseit`.`categories` (
  `categoryid` BIGINT NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(200) NULL,
  `modifiedby` BIGINT NULL,
  `modifiedtime` BIGINT NULL,
  PRIMARY KEY (`categoryid`));

DROP TABLE IF EXISTS `Chooseit`.`Questions`;
CREATE TABLE `Chooseit`.`Questions` (
  `qid` BIGINT NOT NULL AUTO_INCREMENT,
  `question` TEXT NOT NULL,
  `categoryid` BIGINT NULL,
  `score` BIGINT NULL,
  `modifiedby` BIGINT NULL,
  `modifiedtime` BIGINT NULL,
  PRIMARY KEY (`qid`),
  CONSTRAINT `catid`
    FOREIGN KEY (`categoryid`)
    REFERENCES `categories` (`categoryid`));


DROP TABLE IF EXISTS `Chooseit`.`answers`;
CREATE TABLE `Chooseit`.`answers` (
  `ansid` BIGINT NOT NULL AUTO_INCREMENT,
  `qid` BIGINT NOT NULL,
  `answer` TEXT NULL,
  PRIMARY KEY (`ansid`),
  INDEX `qid_idx` (`qid` ASC),
  CONSTRAINT `qid`
    FOREIGN KEY (`qid`)
    REFERENCES `Chooseit`.`questions` (`qid`));


DROP TABLE IF EXISTS correct_answers;

CREATE TABLE `Chooseit`.`correct_answers` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `qid` BIGINT NOT NULL,
  `ansid` BIGINT NOT NULL,
  `modifiedby` BIGINT NULL,
  `modifiedtime` BIGINT NULL,
  PRIMARY KEY (`id`),
  INDEX `qid_idx1` (`qid` ASC),
  INDEX `ansid_idx1` (`ansid` ASC),
  CONSTRAINT `qid1`
    FOREIGN KEY (`qid`)
    REFERENCES `Chooseit`.`questions` (`qid`),
  CONSTRAINT `ansid`
    FOREIGN KEY (`ansid`)
    REFERENCES `Chooseit`.`answers` (`ansid`));
    
    