package com.neo.chooseit;

import com.neo.chooseit.db.Answer;
import com.neo.chooseit.db.Category;
import com.neo.chooseit.db.CorrectAnswer;
import com.neo.chooseit.db.Question;
import org.javalite.activejdbc.Base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by alan-1554 on 04/10/15.
 */
public class ParseDB {
    ParseDB(){
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/Chooseit", "root", "");
    }

    public void loadDB(){
        List<QuestionBean> questionBeanList = new ParseJson().parseJson("questions.json");
        questionBeanList.forEach(questionBean -> {
            new Question()
                     .set("question", questionBean.getQuestion(),
                             "score",questionBean.getScore(),
                             "categoryid", checkNaddCategory(questionBean.getCategory()),
                             "modifiedby", "1",
                             "modifiedtime", Calendar.getInstance().getTimeInMillis()
                     ).saveIt();
            Long qid = Question.findFirst("question = ?", questionBean.getQuestion()).getLong("qid");
            questionBean.getAnswers().forEach(answer -> {
                new Answer().set("qid", qid,
                        "answer", answer).saveIt();
            });
            new CorrectAnswer().set("qid", qid,
                    "ansid", getAnsId(qid, questionBean.getCorrectAnswer()),
                    "modifiedby", 1,
                    "modifiedtime", Calendar.getInstance().getTimeInMillis()).saveIt();
        });
    }

    private Long getAnsId (Long qid, String answer){
        return Answer.findFirst("qid = ? and answer = ?", qid, answer).getLong("ansid");
    }

    private Long getCategoryId(String category){
        return Category.findFirst("category = ?", category).getLong("categoryid");
    }

    public Object checkNaddCategory(String category){
        Object categoryid = new Category().findFirst("category = ?", category);
        if (categoryid == null){
            new Category().set("category", category,
                    "modifiedby", 1,
                    "modifiedtime", Calendar.getInstance().getTimeInMillis()).saveIt();
        }
        categoryid = new Category().findFirst("category = ?", category).get("categoryid");
        return categoryid;
    }

    public List<QuestionBean> parseDB(){
        return parseDB(null);
    }

    public List<QuestionBean> parseDB(String category){
        return parseDB(category, 0);
    }

    public List<QuestionBean> parseDB(String category, int limit){
        return parseDB(category, limit, null);
    }

    public List<QuestionBean> parseDB(String category, int limit, Integer scoreType){
        String where = "";
        Object obj1 = null, obj2 = null;

        if (category != null && !category.isEmpty()){
            where = " categoryid = "+getCategoryId(category);
        }
        if (scoreType!= null && scoreType > 0){
            if (where.isEmpty()){
                where += " score = "+scoreType;
            }else {
                where += " and score = "+scoreType;
            }
        }
        List<QuestionBean> questionBeanList = new ArrayList<>();
        List<Question> questions = null;
        if (limit > 0){
            questions = Question.where(where).limit(limit);
        }else {
            questions = Question.where(where).limit(500);//limit safeguard so as to not crash the app; make it configurable
        }
        questions.forEach(question -> {
            questionBeanList.add(questionToBean(question));
        });
        return questionBeanList;
    }

    private QuestionBean questionToBean(Question question){
        QuestionBean qb = new QuestionBean();
        Long qid = question.getLong("qid");
        qb.setQuestionid(qid);
        qb.setQuestion(question.get("question").toString());
        qb.setScore(question.getInteger("score"));
        qb.setCategory(getCategory(question.getLong("categoryid")));
        qb.setAnswers(getAnswers(qid));
        qb.setCorrectAnswer(getCorrectAnswer(qid));
        return qb;
    }

    private String getCorrectAnswer(Long qid) {
        Long ansid = CorrectAnswer.findFirst("qid = ?", qid).getLong("ansid");
        return Answer.findFirst("ansid = ?", ansid).getString("answer");
    }

    public List<String> getAnswers(Long qid){
        List<Answer> answers = Answer.where("qid = ?", qid);
        List<String> ans = new ArrayList<>();
        answers.forEach(answer -> ans.add(answer.get("answer").toString()));
        return ans;
    }

    public String getCategory(Long catid){
        Object cat = Category.findFirst("categoryid = ?", catid).getString("category");
        return cat == null ? "": cat.toString();
    }

    public List<String> getCategories(){
        List<String> categories = new ArrayList<>();
        Category.findAll().forEach(category -> categories.add(category.getString("category")));
        return categories;
    }

    public List<String> getScoreVal(){
        List<String> scores = new ArrayList<>();
        Question.findBySQL("select distinct score from questions")
                .forEach(score -> scores.add(score.getString("score")));
        return scores;
    }

//    public static void main(String args[]){
//        ParseDB p = new ParseDB();
//        //p.loadDB();
//        //List<QuestionBean> list = p.parseDB("General Knowledge", 2L, 5);
//        //list.forEach(q -> System.out.print(q.toString()));
////        System.out.println(p.getScoreVal());
//        System.out.println(p.getCategories());
//        System.out.println("---------------");
//
//    }
}
